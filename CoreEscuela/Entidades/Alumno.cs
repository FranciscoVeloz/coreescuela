﻿using System;
using System.Collections.Generic;

namespace CoreEscuela.Entidades
{
    public class Alumno
    {
        public string Nombre { get; set; }
        public string IdUnico { get; private set; }
        public List<Evaluaciones> Evaluacion { get; set; } = new List<Evaluaciones>();

        public Alumno() => IdUnico = Guid.NewGuid().ToString();
    }
}
