﻿using System;

namespace CoreEscuela.Entidades
{
    public class Evaluaciones
    {
        public string Nombre { get; set; }
        public string IdUnico { get; private set; }
        public Alumno Alumno { get; set; }
        public Asignatura Asignatura { get; set; }
        public double Nota { get; set; }
        public Evaluaciones() => IdUnico = Guid.NewGuid().ToString();
    }
}
