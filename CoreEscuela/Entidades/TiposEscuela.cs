﻿namespace CoreEscuela.Entidades
{
    public enum TiposEscuela
    {
        Universidad, 
        Maestrias,
        Doctorados
    }
}