﻿using System;

namespace CoreEscuela.Entidades
{
    public class Asignatura
    {
        public string Nombre { get; set; }
        public string IdUnico { get; private set; }

        public Asignatura() => IdUnico = Guid.NewGuid().ToString();
    }
}
