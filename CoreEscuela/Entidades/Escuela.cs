﻿using System;
using System.Collections.Generic;

namespace CoreEscuela.Entidades
{
    public class Escuela
    {
        public string IdUnico { get; set; } = Guid.NewGuid().ToString();
        public string Nombre { get; set; }
        public int AñoDeCreacion { get; set; }
        public string Pais { get; set; }
        public string Ciudad { get; set; }
        public TiposEscuela TipoEscuela { get; set; }

        public List<Curso> Cursos{ get; set; }

        public Escuela(string nombre, TiposEscuela tipo, string lugar, string pais, int año) => (Nombre, TipoEscuela, Ciudad, Pais, AñoDeCreacion) = (nombre, tipo, lugar, pais, año);

        public override string ToString()
        {
            return $"Nombre: {Nombre}, Tipo: {TipoEscuela}, \nCiudad: {Ciudad}, Pais: {Pais}, Año de creacion: {AñoDeCreacion}";
        }
    }
}