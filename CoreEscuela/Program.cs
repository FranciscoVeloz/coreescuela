﻿using System;
using CoreEscuela.App;
using CoreEscuela.Entidades;
using Utilerias;
using static System.Console;

namespace CoreEscuela
{
    class Program
    {
        static void Main(string[] args)
        {
            EscuelaEngine engine = new EscuelaEngine();

            engine.Inicializar();


            ImprimirCursosEscuela(engine.Escuela);
            ReadKey();
        }

        private static void ImprimirCursosEscuela(Escuela escuela)
        {
            Varios.MostrarTitulo("Cursos de la escuela");
            Varios.Mostrar("\n");

            if (escuela?.Cursos != null)
                foreach (var curso in escuela.Cursos)
                {
                    Varios.Mostrar($"\nNombre: {curso.Nombre}, Id: {curso.IdUnico}, Jornada: {curso.Jornada}");
                }
        }
    }
}