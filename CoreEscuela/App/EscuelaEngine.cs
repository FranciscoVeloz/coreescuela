﻿using CoreEscuela.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreEscuela.App
{
    public class EscuelaEngine
    {
        public Escuela Escuela { get; set; }

        public EscuelaEngine()
        {
        }

        //Inicializamos cada uno de los metodos creados
        public void Inicializar()
        {
            Escuela = new Escuela("Tecnologico MM", TiposEscuela.Doctorados, "Lagos de Moreno", "Mexico", 1990);

            CargarCursos();
            CargarAsignaturas();
            CargarEvaluaciones();
        }

        //Cargamos las evaluaciones de cada materia de cada alumno
        private void CargarEvaluaciones()
        {
            foreach (var curso in Escuela.Cursos)
            {
                foreach (var asignatura in curso.Asignaturas)
                {
                    foreach (var alumno in curso.Alumnos)
                    {
                        Random random = new Random();

                        for (int i = 0; i < 5; i++)
                        {
                            var ev = new Evaluaciones
                            {
                                Asignatura = asignatura,
                                Nombre = $"{asignatura.Nombre} Evaluacion#{i + 1}",
                                Nota = 5 * random.NextDouble(),
                                Alumno = alumno
                            };
                            alumno.Evaluacion.Add(ev);
                        }
                    }
                }
            }
        }

        //Cargamos las asignaturas, osea cada materia que se llevaran en la escuela
        private void CargarAsignaturas()
        {
            foreach (var curso in Escuela.Cursos)
            {
                List<Asignatura> listaAsignaturas = new List<Asignatura>()
                {
                    new Asignatura{ Nombre= "Matematicas"},
                    new Asignatura{ Nombre= "Educacion Fisica"},
                    new Asignatura{ Nombre= "Español"},
                    new Asignatura{ Nombre= "Calculo"}
                };
                curso.Asignaturas.AddRange(listaAsignaturas);
            }
        }

        //Cargamos los alumnos que estaran en nuestra escuela
        private List<Alumno> GenerarAlumnosAleatorios(int cantidad)
        {
            string[] nombre = {"Francisco", "Daneli", "Carlos", "John"};
            string[] apellido1 = { "Gonzalez", "Reyes", "Fernandez", "Freddy" };
            string[] apellido2 = { "Veloz", "Gonzalez", "Cervantes", "Vega" };

            var listaAlumnos = from n1 in nombre
                               from ap1 in apellido1
                               from ap2 in apellido2
                               select new Alumno { Nombre = $"{n1} {ap1} {ap2}" };

            return listaAlumnos.OrderBy((alumnos) => alumnos.IdUnico).Take(cantidad).ToList();   
        }

        //Cargamos cada uno de los grupos que estaran en nuestra escuela
        private void CargarCursos()
        {
            Escuela.Cursos = new List<Curso>()
            {
                new Curso(){ Nombre = "101", Jornada = TiposJornada.Mañana},
                new Curso(){ Nombre = "201", Jornada = TiposJornada.Mañana},
                new Curso(){ Nombre = "301", Jornada = TiposJornada.Mañana},
                new Curso(){ Nombre = "401", Jornada = TiposJornada.Tarde},
                new Curso(){ Nombre = "501", Jornada = TiposJornada.Tarde}
            };

            Random aleatorio = new Random();

            foreach (var curso in Escuela.Cursos)
            {
                int cantidadAlumnos = aleatorio.Next(5, 20);
                curso.Alumnos = GenerarAlumnosAleatorios(cantidadAlumnos);
            }
        }
    }
}
